const readMoreBtns = document.querySelectorAll(".read-more-btn");
const texts = document.querySelectorAll(".text");

readMoreBtns.forEach((readMoreBtn, index) => {
  readMoreBtn.addEventListener("click", () => {
    texts[index].classList.toggle("show-more");
    if (readMoreBtn.innerText === "Read More") {
      readMoreBtn.innerText = "Read Less";
    } else {
      readMoreBtn.innerText = "Read More";
    }
  });
});

const images = [
  "Img/cancer.jpg",
  "Img/Thyroid gland .jpg",
  "Img/Pregnancy and contraception.jpg",
  "Img/photo_2024-01-14_21-41-26.jpg",
];

let currentImageIndex = 0;
const intervalTime = 3000; // بالميلي ثانية
let slideInterval;

function showNextImage() {
  const sliderImage = document.querySelector(".slider-image");
  sliderImage.src = images[currentImageIndex];
  currentImageIndex = (currentImageIndex + 1) % images.length;
  updateNavigationDots();
}

function createNavigationDots() {
  const navigationDots = document.querySelector(".navigation-dots");

  images.forEach((_image, index) => {
    const dot = document.createElement("span");
    dot.classList.add("dot");
    dot.addEventListener("click", () => {
      currentImageIndex = index;
      showNextImage();
    });
    navigationDots.appendChild(dot);
  });

  updateNavigationDots();
}

function updateNavigationDots() {
  const dots = document.querySelectorAll(".dot");
  dots.forEach((dot, index) => {
    dot.classList.toggle("active", index === currentImageIndex);
  });
}

function startSlider() {
  slideInterval = setInterval(showNextImage, intervalTime);
}

function stopSlider() {
  clearInterval(slideInterval);
}

createNavigationDots();
startSlider();
